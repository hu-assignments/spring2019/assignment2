package util;

import restaurant.Item;

import java.util.Set;

public final class Util {

    /**
     * Returns the maximum length of the names of the {@code Item}s to
     * be able to correctly align them in the output.
     *
     * @param items The {@code Set} of {@code Item}s.
     * @return {@code int}.
     */
    public static int maxNameLength(Set<Item> items) {
        int max = 0;
        for (Item item : items) {
            if (item.getName().length() > max) {
                max = item.getName().length();
            }
        }
        return max;
    }

    /**
     * Justifies the {@code String} by adding padding to the left of it.
     *
     * @param string The {@code String} to be justified.
     * @param amount The padding amount.
     * @return The justified {@code String}.
     */
    public static String leftJustify(String string, int amount) {
        return String.format("%-" + amount + "s", string);
    }

    public static String pluralize(int count) {
        return (count > 1 || count == 0) ? "s" : "";
    }
}
