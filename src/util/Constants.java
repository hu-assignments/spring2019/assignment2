package util;

public final class Constants {
    public static final String SETUP_FILENAME = "build/production/assignment2/setup.dat";
    public static final String COMMANDS_FILENAME = "build/production/assignment2/commands.dat";

    public static final String CMD_SEPARATOR = " ";
    public static final String ATTR_SEPARATOR = ";";
    public static final String ITEM_SEPARATOR = ":";
    public static final String ITEM_AMOUNT_SEPARATOR = "-";

    public static final String ADD_ITEM = "add_item";
    public static final String ADD_EMPLOYER = "add_employer";
    public static final String ADD_WAITER = "add_waiter";

    public static final String CREATE_TABLE = "create_table";
    public static final String NEW_ORDER = "new_order";
    public static final String ADD_ORDER = "add_order";
    public static final String CHECK_OUT = "check_out";
    public static final String STOCK_STATUS = "stock_status";
    public static final String GET_ORDER_STATUS = "get_order_status";
    public static final String GET_TABLE_STATUS = "get_table_status";
    public static final String GET_EMPLOYER_SALARY = "get_employer_salary";
    public static final String GET_WAITER_SALARY = "get_waiter_salary";

    public static final int MAX_WAITER = 5;
    public static final int MAX_EMPLOYER = 5;
    public static final int MAX_TABLE = 5;
    public static final int MAX_TABLE_PER_EMPLOYER = 2;
    public static final int MAX_TABLE_PER_WAITER = 3;
    public static final int MAX_ORDER = 5;
    public static final int MAX_ITEM_PER_ORDER = 10;
}
