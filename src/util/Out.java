package util;

public final class Out {

    public static void println(Object object) {
        System.out.println(object.toString());
    }

    public static void printProcessingCommand(String command) {
        System.out.println("***********************************");
        System.out.println(String.format("PROGRESSING COMMAND: %s", command));
    }

}