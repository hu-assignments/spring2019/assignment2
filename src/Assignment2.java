import input.CommandExecutor;
import input.RestaurantBuilder;
import restaurant.Restaurant;
import util.Out;

import static util.Constants.COMMANDS_FILENAME;
import static util.Constants.SETUP_FILENAME;

public final class Assignment2 {
    public static void main(String[] args) {
        try {
            Restaurant restaurant = new RestaurantBuilder(SETUP_FILENAME).build();
            CommandExecutor commandExecutor = new CommandExecutor(COMMANDS_FILENAME, restaurant);
            commandExecutor.startExecution();
        } catch (Exception e) {
            Out.println(e.getMessage());
            Out.println("Stack trace:");
            e.printStackTrace();
        }
    }
}