package restaurant;

import exceptions.TooManyTablesException;

/**
 * The interface to implement ownership of the {@code Table}s for each type of {@code Worker}.
 */
public interface TablePossession {
    void assignTableToWorker(Table table) throws TooManyTablesException;
}
