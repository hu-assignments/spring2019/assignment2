package restaurant;

import exceptions.*;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import static util.Constants.*;

public class Restaurant {

    private final ArrayList<Item> mStock;
    private final ArrayList<Employer> mEmployers;
    private final ArrayList<Waiter> mWaiters;
    private final ArrayList<Table> mTables;
    private Set<Item> mUniqueStockItemsOnSetup;

    public Restaurant() {
        this.mStock = new ArrayList<>();
        this.mEmployers = new ArrayList<>();
        this.mWaiters = new ArrayList<>();
        this.mTables = new ArrayList<>();
        this.mUniqueStockItemsOnSetup = new LinkedHashSet<>();
    }

    public void initializeUniqueStockItemsOnSetup() {
        this.mUniqueStockItemsOnSetup = new LinkedHashSet<>(getStock());
    }

    /**
     * Gets the unique {@code Item}s from the stock.
     *
     * @return {@code Set<Item>} of unique {@code Item}s in the stock.
     */
    public Set<Item> getUniqueStockItemsOnSetup() {
        return this.mUniqueStockItemsOnSetup;
    }

    public ArrayList<Table> getTables() {
        return this.mTables;
    }

    public ArrayList<Employer> getEmployers() {
        return this.mEmployers;
    }

    public ArrayList<Waiter> getWaiters() {
        return this.mWaiters;
    }

    /**
     * Gets {@code Item} with the given name from the stock.
     *
     * @param name Name of the stock.
     * @return {@code Item}.
     * @throws NoItemInStockException When no {@code Item} with supplied name can be found in stock.
     * @throws NoSuchItemException    When no {@code Item} with the supplied name is unknown.
     */
    public Item getItem(String name) throws NoItemInStockException, NoSuchItemException {
        if (getUniqueStockItemsOnSetup().stream().
                filter(item -> item.getName().equalsIgnoreCase(name)).findAny().orElse(null) == null)
            throw new NoSuchItemException(name);
        for (Item item : this.mStock) {
            if (item.getName().equalsIgnoreCase(name)) {
                return item;
            }
        }
        throw new NoItemInStockException(name);
    }

    /**
     * Brings an empty {@code Table} with given ID from the {@code Restaurant}.
     *
     * @param id   ID of the requested {@code Table}.
     * @param name Name of the {@code Waiter}.
     * @return {@code Table}.
     * @throws NoSuchTableException       When no {@code Table} with given ID can be found.
     * @throws TableOutOfServiceException When the {@code Table} is occupied.
     */
    public Table getTableInService(int id, String name) throws NoSuchTableException, TableOutOfServiceException {
        for (Table table : getTables()) {
            if (table.getID() == id) {
                if (table.isInService())
                    return table;
                throw new TableOutOfServiceException(name);
            }
        }
        throw new NoSuchTableException(id);
    }


    /**
     * Adds {@code Table} to {@code Restaurant}.
     *
     * @param table    The {@code Table} to be added.
     * @param employer The {@code Employer} that will assigned to this {@code Table}.
     * @throws TooManyTablesException When maximum number of {@code Table}s reached
     *                                both for {@code Employer} and the {@code Restaurant}.
     */
    public void addTable(Table table, Employer employer) throws TooManyTablesException {
        if (this.mTables.size() + 1 > MAX_TABLE)
            throw new TooManyTablesException(MAX_TABLE);
        employer.assignTableToWorker(table);
        this.mTables.add(table);
        table.initID();
    }

    /**
     * Assigns the supplied {@code Table} to the {@code Waiter}.
     *
     * @param table  The {@code Table} that will be assigned.
     * @param waiter The {@code Table} will be assigned to this {@code Waiter}.
     * @throws TooManyTablesException When the {@code Waiter} has reached it's maximum
     *                                number of {@code Table} it can operate.
     */
    public void assignTable(Table table, Waiter waiter) throws TooManyTablesException {
        table.assignWaiter(waiter);
    }

    /**
     * Gets the appropriate {@code Table} from the {@code Restaurant} with supplied minimum capacity.
     *
     * @param minCapacity Minimum capacity that the {@code Table} should have.
     * @return The appropriate {@code Table}.
     * @throws NoEligibleTableFoundException When no appropriate {@code Table} can be found.
     */
    public Table getEligibleTable(int minCapacity) throws NoEligibleTableFoundException {
        for (Table table : getTables()) {
            if ((!table.isInService()) && table.getCapacity() >= minCapacity) {
                return table;
            }
        }
        throw new NoEligibleTableFoundException();
    }

    /**
     * Returns the {@code Item} from the {@code Table}. Then removes it from the stock.
     *
     * @param itemName Name of the {@code Item} that should be returned.
     * @return {@code Item}.
     * @throws NoItemInStockException When no {@code Item} with the supplied name can be found in stock.
     * @throws NoSuchItemException    When no {@code Item} with the supplied name is unknown.
     */
    public Item bringItemFromStock(String itemName) throws NoItemInStockException, NoSuchItemException {
        Item item = getItem(itemName);
        removeItemFromStock(item);
        return item;
    }

    private void removeItemFromStock(Item item) {
        this.mStock.remove(item);
    }

    public void addItemToStock(Item item) {
        this.mStock.add(item);
    }

    public ArrayList<Item> getStock() {
        return this.mStock;
    }

    /**
     * Gets the {@code Employer} with the supplied name.
     *
     * @param name Name of the {@code Employer}.
     * @return {@code Employer}.
     * @throws NoSuchWorkerException When no {@code Employer} with the supplied name can be found.
     */
    public Employer getEmployerByName(String name) throws NoSuchWorkerException {
        for (Employer employer : this.mEmployers) {
            if (employer.getName().equalsIgnoreCase(name))
                return employer;
        }
        throw new NoSuchWorkerException("employer", name);
    }

    /**
     * Gets the {@code Waiter} with the supplied name.
     *
     * @param name Name of the {@code Waiter}.
     * @return {@code Waiter}.
     * @throws NoSuchWorkerException When no {@code Waiter} with the supplied name can be found.
     */
    public Waiter getWaiterByName(String name) throws NoSuchWorkerException {
        for (Waiter waiter : this.mWaiters) {
            if (waiter.getName().equalsIgnoreCase(name))
                return waiter;
        }
        throw new NoSuchWorkerException("waiter", name);
    }

    /**
     * Adds a new {@code Employer} to the {@code Restaurant}.
     *
     * @param employer The {@code Employer} to be added.
     * @throws WorkerAlreadyExistsException When an {@code Employer} with the same name already
     *                                      exists in the {@code Restaurant}.
     * @throws TooManyWorkersException      When maximum number of {@code Employer}s has already been reached.
     */
    public void addEmployer(Employer employer) throws WorkerAlreadyExistsException, TooManyWorkersException {
        if (this.mEmployers.size() < MAX_EMPLOYER) {
            if (!this.mEmployers.contains(employer))
                this.mEmployers.add(employer);
            else
                throw new WorkerAlreadyExistsException("employer", employer.getName());
        } else
            throw new TooManyWorkersException("employers", MAX_EMPLOYER);
    }

    /**
     * Adds a new {@code Waiter} to the {@code Restaurant}.
     *
     * @param waiter The {@code Waiter} to be added.
     * @throws WorkerAlreadyExistsException When a {@code Waiter} with the same name already
     *                                      exists in the {@code Restaurant}.
     * @throws TooManyWorkersException      When maximum number of {@code Waiter}s has already been reached.
     */
    public void addWaiter(Waiter waiter) throws WorkerAlreadyExistsException, TooManyWorkersException {
        if (this.mWaiters.size() < MAX_WAITER) {
            if (!this.mWaiters.contains(waiter))
                this.mWaiters.add(waiter);
            else
                throw new WorkerAlreadyExistsException("waiter", waiter.getName());
        } else
            throw new TooManyWorkersException("waiters", MAX_WAITER);
    }
}