package restaurant;

import java.util.ArrayList;

/**
 * Super class of the {@code Waiter} and the {@code Employer}.
 *
 * <p>Contains the name, salary, and {@code Table} information.
 */
public class Worker {
    private String mName;
    private double mSalary;
    private ArrayList<Table> mTables;


    /**
     * Constructs a new {@code Worker} with {@code String} representation of the {@code Worker}.
     *
     * @param attributes {@code String[]} of attributes.
     */
    public Worker(String[] attributes) {
        setName(attributes[0].trim());
        setSalary(Double.valueOf(attributes[1]));
        this.mTables = new ArrayList<>();
    }


    public ArrayList<Table> getTables() {
        return this.mTables;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    /**
     * Gets the base salary for this {@code Worker}.
     *
     * @return {@code double}, adjusted salary.
     */
    public double getSalary() {
        return this.mSalary;
    }

    public void setSalary(double salary) {
        this.mSalary = salary;
    }
}