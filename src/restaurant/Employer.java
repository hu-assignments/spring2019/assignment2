package restaurant;

import exceptions.TooManyTablesException;

import java.util.ArrayList;

import static util.Constants.MAX_TABLE_PER_EMPLOYER;

public class Employer extends Worker implements TablePossession {

    private static final double AWARD_COEFFICIENT = 0.1D;
    private int mTotalNumberOfTablesCreated = 0;

    /**
     * Constructs a new {@code Employer} with {@code String} representation of the {@code Employer}.
     *
     * @param attributes {@code String[]} of attributes.
     */
    public Employer(String[] attributes) {
        super(attributes);
    }


    /**
     * Assigns the supplied {@code Table} to this {@code Employer}. Which means
     * that this {@code Employer} has now permission to operate on the {@code Table}.
     *
     * @param table The {@code Table} to be added to this {@code Employer}s {@code Table} array.
     * @throws TooManyTablesException When the maximum number of {@code Table}s can be assigned
     *                                to this {@code Employer} has already been reached.
     */
    @Override
    public final void assignTableToWorker(Table table) throws TooManyTablesException {
        ArrayList<Table> tables = this.getTables();
        if (tables.size() + 1 > MAX_TABLE_PER_EMPLOYER)
            throw new TooManyTablesException(this, MAX_TABLE_PER_EMPLOYER);
        tables.add(table);
        mTotalNumberOfTablesCreated++;
    }

    /**
     * Gets the adjusted salary for this {@code Employer}.
     *
     * @return {@code double}, adjusted salary.
     */
    @Override
    public double getSalary() {
        return super.getSalary() + super.getSalary() * mTotalNumberOfTablesCreated * AWARD_COEFFICIENT;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Employer) {
            return this.getName().equalsIgnoreCase(((Employer) o).getName());
        }
        return false;
    }
}