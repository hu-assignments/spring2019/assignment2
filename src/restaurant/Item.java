package restaurant;

public class Item {
    private String mName;
    private double mPrice;

    /**
     * Constructs a new {@code Item} with {@code String} representation of the {@code Item}.
     *
     * @param attributes {@code String[]} of attributes.
     */
    public Item(String[] attributes) {
        setName(attributes[0]);
        setPrice(Double.valueOf(attributes[1]));
    }

    public double getPrice() {
        return this.mPrice;
    }


    public void setPrice(double price) {
        this.mPrice = price;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Item) {
            return ((Item) o).getName().equalsIgnoreCase(this.getName())
                    && ((Item) o).getPrice() == this.getPrice();
        }
        return false;
    }

    /**
     * To be able to create a {@code Set} from the {@code ArrayList} of {@code Item}s,
     * every {@code Item} should have a unique hashcode.
     *
     * @return {@code int} hashcode.
     */
    @Override
    public int hashCode() {
        return getName().concat(String.valueOf(getPrice())).hashCode();
    }
}