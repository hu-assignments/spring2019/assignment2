package restaurant;

import exceptions.TooManyOrdersException;
import exceptions.TooManyTablesException;

import java.util.ArrayList;

import static util.Constants.MAX_ORDER;

public class Table {

    private static int sTableCount = 0;
    private int mID;
    private int mCapacity;
    private Waiter mWaiter;
    private boolean mInService;
    private ArrayList<Order> mOrders;

    public Table(int capacity) {
        this.mCapacity = capacity;
        this.mInService = false;
        this.mOrders = new ArrayList<>();
    }

    public void initID() {
        mID = sTableCount++;
    }

    public void addOrder(Order order) throws TooManyOrdersException {
        if (getOrders().size() + 1 > MAX_ORDER)
            throw new TooManyOrdersException();
        getOrders().add(order);
    }

    /**
     * Sets the state of service of the {@code Table}.
     *
     * @param state The new state.
     */
    public void setServiceState(boolean state) {
        this.mInService = state;
    }

    public boolean isInService() {
        return this.mInService;
    }

    public int getID() {
        return this.mID;
    }

    public int getCapacity() {
        return this.mCapacity;
    }

    public Waiter getWaiter() {
        return this.mWaiter;
    }

    public ArrayList<Order> getOrders() {
        return this.mOrders;
    }

    public void assignWaiter(Waiter waiter) throws TooManyTablesException {
        waiter.assignTableToWorker(this);
        this.mWaiter = waiter;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Table) {
            return ((Table) o).getID() == this.getID();
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("Table %d: %s", getID(), (isInService()) ? String.format("Reserved (%s)", getWaiter().getName()) : "Free");
    }
}