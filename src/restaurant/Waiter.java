package restaurant;

import exceptions.AuthException;
import exceptions.TableOutOfServiceException;
import exceptions.TooManyOrdersException;
import exceptions.TooManyTablesException;
import util.Out;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import static util.Constants.MAX_TABLE_PER_WAITER;

public class Waiter extends Worker implements TablePossession {

    private static final double AWARD_COEFFICIENT = 0.05D;
    private int mTotalNumberOfOrdersManaged = 0;

    /**
     * Constructs a new {@code Waiter} with {@code String} representation of the {@code Waiter}.
     *
     * @param attributes {@code String[]} of attributes.
     */
    public Waiter(String[] attributes) {
        super(attributes);
    }

    /**
     * Assigns the supplied {@code Table} to this {@code Waiter}. Which means
     * that this {@code Waiter} has now permission to operate on the {@code Table}.
     *
     * @param table The {@code Table} to be added to this {@code Waiter}s {@code Table} array.
     * @throws TooManyTablesException When the maximum number of {@code Table}s can be assigned
     *                                to this {@code Waiter} has already been reached.
     */
    @Override
    public final void assignTableToWorker(Table table) throws TooManyTablesException {
        ArrayList<Table> tables = this.getTables();
        if (tables.size() + 1 > MAX_TABLE_PER_WAITER)
            throw new TooManyTablesException(this, MAX_TABLE_PER_WAITER);
        tables.add(table);
    }

    /**
     * Adds a new {@code Order} to the {@code Table}.
     *
     * @param table The {@code Table} that will receive the new {@code Order}.
     * @param order The {@code Order} that should be added to the {@code Table}.
     * @throws AuthException          When the {@code Waiter} does not havea permission
     *                                to operate on the {@code Table}.
     * @throws TooManyOrdersException When maximum number of {@code Order}s has already been reached.
     */
    public void addOrder(Table table, Order order) throws AuthException, TooManyOrdersException {
        if (!hasAuthOn(table))
            throw new AuthException(getName());
        table.addOrder(order);
        mTotalNumberOfOrdersManaged++;
    }

    /**
     * Returns whether the {@code Waiter} has authorization or not by looking up the
     * {@code Table}s they have.
     *
     * @param table The {@code Table} that will be checked whether the {@code Waiter} has permission or not.
     * @return {@code boolean}, the {@code Waiter} has permission or not.
     */
    public boolean hasAuthOn(Table table) {
        return getTables().contains(table);
    }

    /**
     * Performs a checkout operation on the {@code Table}.
     * Then removes all {@code Order}s from the {@code Table}, takes it out
     * of service, and detaches it from the {@code Waiter}.
     *
     * @param table The {@code Table} that a checkout operation will be realized.
     * @throws AuthException              When the {@code Waiter} does not have a permission
     *                                    to operate on this {@code Table}.
     * @throws TableOutOfServiceException When the {@code Table} is already out of service.
     */
    public void performCheckout(Table table) throws AuthException, TableOutOfServiceException {
        if (!table.isInService()) throw new TableOutOfServiceException(getName());
        if (!hasAuthOn(table))
            throw new AuthException(getName());

        ArrayList<Item> orderedItems = new ArrayList<>();
        for (Order order : table.getOrders()) {
            orderedItems.addAll(order.getItems());
        }

        double total = 0d;
        Set<Item> uniqueItems = new LinkedHashSet<>(orderedItems);

        for (Item item : uniqueItems) {
            int amount = Collections.frequency(orderedItems, item);
            double price = item.getPrice();
            double totalCostForThisItem = price * amount;
            Out.println(String.format("%s:\t%.3f (x %d) %.3f $",
                    item.getName(), price, amount, totalCostForThisItem));
            total += totalCostForThisItem;
        }

        Out.println(String.format("Total:\t%.3f $", total));

        table.getOrders().clear();
        table.setServiceState(false);
        getTables().remove(table);
    }

    /**
     * Gets the adjusted salary for this {@code Waiter}.
     *
     * @return {@code double}, adjusted salary.
     */
    @Override
    public double getSalary() {
        return super.getSalary() + super.getSalary() * mTotalNumberOfOrdersManaged * AWARD_COEFFICIENT;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Waiter) {
            return this.getName().equalsIgnoreCase(((Waiter) o).getName());
        }
        return false;
    }
}