package restaurant;

import exceptions.TooManyItemsException;

import java.util.ArrayList;

import static util.Constants.MAX_ITEM_PER_ORDER;

public class Order {
    private ArrayList<Item> mItems;

    public Order() {
        this.mItems = new ArrayList<>();
    }

    /**
     * Adds an {@code Item} to this {@code Order}.
     *
     * @param item The {@code Item} to be added.
     * @throws TooManyItemsException When maximum number of {@code Item}s per single
     *                               {@code Order} has already been reached.
     */
    public void addItemToOrder(Item item) throws TooManyItemsException {
        if (getItems().size() + 1 > MAX_ITEM_PER_ORDER)
            throw new TooManyItemsException();
        getItems().add(item);
    }

    public ArrayList<Item> getItems() {
        return this.mItems;
    }
}

