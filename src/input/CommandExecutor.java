package input;

import exceptions.*;
import restaurant.*;
import util.Out;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;

import static util.Constants.*;

public class CommandExecutor {

    private static final String SEPARATOR = " ";

    private String mFilename;
    private Restaurant mRestaurant;

    /**
     * Executes commands in the file with given name.
     *
     * @param filename   Name or path of the file that contains commands to operate the Restaurant.
     * @param restaurant The Restaurant object to operate on.
     */
    public CommandExecutor(String filename, Restaurant restaurant) {
        this.setRestaurant(restaurant);
        this.setFilename(filename);
    }

    /**
     * Inserts a new {@code Table} in {@code Restaurant}.
     * After table creation, the table will be assigned to the Employer.
     *
     * @param employerName Name of the {@code Employer} that will create the {@code Table}.
     * @param capacity     Capacity of the {@code Table}.
     */
    private void createTable(String employerName, int capacity) throws TooManyTablesException, NoSuchWorkerException {
        Employer employer = getRestaurant().getEmployerByName(employerName);
        Table table = new Table(capacity);
        getRestaurant().addTable(table, employer);

        Out.println("A new table has successfully been added");
    }

    /**
     * Places new customers into an appropriate {@code Table}, and takes {@code Order}s.
     * The {@code Table} then, would be assigned to the {@code Waiter}.
     *
     * @param waiterName    Name of the {@code Waiter} that will perform this action.
     * @param customerCount Number of customers to place in {@code Table}.
     * @param items         {@code String[]} of {@code String} representation of {@code Items}s to be ordered.
     */
    private void newOrder(String waiterName, int customerCount, String[] items) throws NoSuchWorkerException, NoEligibleTableFoundException, TooManyTablesException, AuthException, TooManyOrdersException {
        Waiter waiter = getRestaurant().getWaiterByName(waiterName);
        Table eligibleTable = getRestaurant().getEligibleTable(customerCount);

        getRestaurant().assignTable(eligibleTable, waiter);
        eligibleTable.setServiceState(true);

        Out.println(String.format("Table (= ID %d) has been taken into service", eligibleTable.getID()));

        Order order = new Order();
        waiter.addOrder(eligibleTable, order);

        for (String itemStr : items) { // itemStr = "Pizza-5"
            String itemName = itemStr.split(ITEM_AMOUNT_SEPARATOR)[0];
            int orderAmount = Integer.valueOf(itemStr.split(ITEM_AMOUNT_SEPARATOR)[1]);

            while (orderAmount-- > 0) {
                try {
                    order.addItemToOrder(getRestaurant().bringItemFromStock(itemName));
                    Out.println(String.format("Item %s added into order", itemName));
                } catch (NoItemInStockException | NoSuchItemException | TooManyItemsException e) {
                    Out.println(e.getMessage());
                }
            }
        }
    }

    /**
     * Adds a new {@code Order} to a {@code Table} with existent customers.
     *
     * @param requestedWaiterName Name of the {@code Waiter} that should perform this operation.
     * @param tableID             ID of the {@code Table}.
     * @param itemsToAdd          {@code String[]} representation of the items.
     */
    private void addOrder(String requestedWaiterName, int tableID, String[] itemsToAdd) throws NoSuchWorkerException, NoSuchTableException, TableOutOfServiceException, AuthException, TooManyOrdersException {
        Waiter requestedWaiter = getRestaurant().getWaiterByName(requestedWaiterName);
        Table tableToAddOrder = getRestaurant().getTableInService(tableID, requestedWaiterName);

        Order order = new Order();
        requestedWaiter.addOrder(tableToAddOrder, order);

        for (String itemStr : itemsToAdd) {
            String itemName = itemStr.split(ITEM_AMOUNT_SEPARATOR)[0];
            int orderAmount = Integer.valueOf(itemStr.split(ITEM_AMOUNT_SEPARATOR)[1]);

            while (orderAmount-- > 0) {
                try {
                    order.addItemToOrder(getRestaurant().bringItemFromStock(itemName));
                    Out.println(String.format("Item %s added into order", itemName));
                } catch (NoItemInStockException | NoSuchItemException | TooManyItemsException e) {
                    Out.println(e.getMessage());
                }
            }
        }
    }

    /**
     * Prints all ordered {@code Item}s, and total cost.
     * Clears the {@code Order}s on the {@code Table}, takes it out of service.
     *
     * @param waiterName Name of the {@code Waiter} that will perform this operation.
     * @param tableID    ID of the {@code Table}.
     */
    private void checkout(String waiterName, int tableID) throws NoSuchWorkerException, NoSuchTableException, TableOutOfServiceException, AuthException {
        Waiter waiter = getRestaurant().getWaiterByName(waiterName);
        Table table = getRestaurant().getTableInService(tableID, waiterName);

        waiter.performCheckout(table);
    }

    /**
     * Prints the stock status of the {@code Restaurant} in insertion order.
     *
     * <p>Inıtially, it was aligning it without using tabs to make the misalignment impossible.
     * But to match with sample output format, I used tabs.
     */
    private void printStockStatus() {
        for (Item item : getRestaurant().getUniqueStockItemsOnSetup()) {
            int amount = Collections.frequency(getRestaurant().getStock(), item);
            Out.println(String.format("%s\t%d", item.getName() + ":", amount));
        }
    }

    /**
     * Prints the status of all the {@code Table}s of the {@code Restaurant}.
     */
    private void printTableStatus() {
        for (Table table : getRestaurant().getTables()) {
            Out.println(table);
        }
    }

    /**
     * Prints the {@code Order} status of each {@code Table}.
     */
    private void printOrderStatus() {
        for (Table table : getRestaurant().getTables()) {
            Out.println(String.format("Table: %d", table.getID()));
            int numberOfOrders = table.getOrders().size();
            Out.println(String.format("\t%d order(s)", numberOfOrders));
            for (Order order : table.getOrders()) {
                int numberOfItems = order.getItems().size();
                Out.println(String.format("\t\t%d item(s)", numberOfItems));
            }
        }
    }

    /**
     * Prints the salaries of the {@code Employer}s.
     *
     * <p>Aligns it without using tabs to make the misalignment impossible.
     */
    private void printEmployerSalaries() {
        if (getRestaurant().getEmployers().size() > 0) {
            for (Employer employer : getRestaurant().getEmployers()) {
                Out.println(String.format("Salary for %s: %.1f", employer.getName(), employer.getSalary()));
            }
        } else Out.println("No employer registered in the Restaurant.");
    }

    /**
     * Prints the salaries of the {@code Waiter}s.
     *
     * <p>Aligns it without using tabs to make the misalignment impossible.
     */
    private void printWaiterSalaries() {
        if (getRestaurant().getWaiters().size() > 0) {
            for (Waiter waiter : getRestaurant().getWaiters()) {
                Out.println(String.format("Salary for %s: %.1f", waiter.getName(), waiter.getSalary()));
            }
        } else Out.println("No waiter registered in the Restaurant.");
    }

    /**
     * Executes commands.
     *
     * @param command The given command.
     * @throws TooManyTablesException        When a {@code Worker} tries to create more {@code Table}s than allowed number.
     * @throws NoSuchWorkerException         When a command tries to assign a work to a non-existent {@code Worker}.
     * @throws TooManyOrdersException        When number of {@code Order}s in a single {@code Table} is about
     *                                       to exceed maximum number.
     * @throws AuthException                 When a {@code Waiter} tries to operate on a {@code Table} with no permission.
     * @throws NoEligibleTableFoundException When no appropriate {@code Table} was found for the {@code Order}.
     * @throws NoSuchTableException          When no {@code Table} with supplied ID can be found.
     * @throws TableOutOfServiceException    When a {@code Waiter} tries to add order, or perform checkout
     *                                       on a {@code Table} that is out of service.
     */
    private void executeCommand(String command) throws TooManyTablesException, NoSuchWorkerException, TooManyOrdersException, AuthException, NoEligibleTableFoundException, NoSuchTableException, TableOutOfServiceException {
        String baseCommand = command.split(SEPARATOR)[0].trim();
        String stringAttributes = command.substring(command.indexOf(SEPARATOR) + SEPARATOR.length()).trim();
        String[] attributes = stringAttributes.split(ATTR_SEPARATOR);

        Out.printProcessingCommand(baseCommand);

        switch (baseCommand) {
            case CREATE_TABLE: // create_table [EMPLOYER NAME];[CAPACITY]
                String employerName = attributes[0];
                int capacity = Integer.valueOf(attributes[1]);

                createTable(employerName, capacity);
                return;
            case NEW_ORDER: // new_order [WAITER NAME];[#CUSTOMER];[ITEM NAME]-[ORDER COUNT]
                String waiterName = attributes[0];
                int customerCount = Integer.valueOf(attributes[1]);
                String[] items = attributes[2].split(ITEM_SEPARATOR);

                newOrder(waiterName, customerCount, items);
                return;
            case ADD_ORDER: // add_order [WAITER NAME];[TABLE ID];[ITEM NAME]-[ORDER COUNT]:
                String requestedWaiterName = attributes[0];
                int tableID = Integer.valueOf(attributes[1]);
                String[] itemsToAdd = attributes[2].split(ITEM_SEPARATOR);

                addOrder(requestedWaiterName, tableID, itemsToAdd);
                return;
            case CHECK_OUT: // check_out [WAITER NAME];[TABLE ID]:
                String checkoutPerformerName = attributes[0];
                int checkoutTableID = Integer.valueOf(attributes[1]);

                checkout(checkoutPerformerName, checkoutTableID);
                return;
            case STOCK_STATUS:
                printStockStatus();
                return;
            case GET_TABLE_STATUS:
                printTableStatus();
                return;
            case GET_ORDER_STATUS:
                printOrderStatus();
                return;
            case GET_EMPLOYER_SALARY:
                printEmployerSalaries();
                return;
            case GET_WAITER_SALARY:
                printWaiterSalaries();
                return;
        }

        throw new InvalidCommandException(command);
    }

    /**
     * Starts the execution of the commands line by line.
     *
     * @throws IOException When no file with specified name can be found, or another IO exception occurs.
     */
    public void startExecution() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(this.getFilename()));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            try {
                if (line.length() > 0)
                    this.executeCommand(line);
            } catch (IllegalArgumentException | RestaurantException e) {
                Out.println(e.getMessage());
            }
        }

        bufferedReader.close();
    }

    private Restaurant getRestaurant() {
        return this.mRestaurant;
    }

    private void setRestaurant(Restaurant restaurant) {
        this.mRestaurant = restaurant;
    }

    private String getFilename() {
        return this.mFilename;
    }

    private void setFilename(String filename) {
        this.mFilename = filename;
    }
}