package input;

import exceptions.InvalidCommandException;
import exceptions.RestaurantException;
import exceptions.TooManyWorkersException;
import exceptions.WorkerAlreadyExistsException;
import restaurant.Employer;
import restaurant.Item;
import restaurant.Restaurant;
import restaurant.Waiter;
import util.Out;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static util.Constants.*;

public class RestaurantBuilder {

    private String mSetupFileName;
    private Restaurant mRestaurant;

    public RestaurantBuilder(String setupFileName) {
        this.mSetupFileName = setupFileName;
        this.mRestaurant = new Restaurant();
    }

    private void addEmployerToRestaurant(Employer employer) throws WorkerAlreadyExistsException, TooManyWorkersException {
        this.mRestaurant.addEmployer(employer);
    }

    private void addWaiterToRestaurant(Waiter waiter) throws WorkerAlreadyExistsException, TooManyWorkersException {
        this.mRestaurant.addWaiter(waiter);
    }

    private Restaurant getRestaurant() {
        return this.mRestaurant;
    }

    private void executeSetupCommand(String command) throws RestaurantException {
        String baseCommand = command.split(CMD_SEPARATOR)[0].trim();
        String stringAttributes = command.substring(command.indexOf(CMD_SEPARATOR) + CMD_SEPARATOR.length()).trim();
        String[] attributes = stringAttributes.split(ATTR_SEPARATOR);

        switch (baseCommand) {
            case ADD_ITEM:
                int amount = Integer.valueOf(attributes[2]);
                while (amount-- > 0) {
                    Item item = new Item(attributes);
                    getRestaurant().addItemToStock(item);
                }
                getRestaurant().initializeUniqueStockItemsOnSetup();
                return;
            case ADD_EMPLOYER:
                Employer employer = new Employer(attributes);
                this.addEmployerToRestaurant(employer);
                return;
            case ADD_WAITER:
                Waiter waiter = new Waiter(attributes);
                this.addWaiterToRestaurant(waiter);
                return;
        }
        throw new InvalidCommandException(command);
    }

    /**
     * Builds the {@code Restaurant} with the commands in setup file.
     *
     * @return {@code Restaurant}.
     * @throws IOException When an error occurs while reading the setup file.
     */
    public Restaurant build() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(this.mSetupFileName));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            try {
                if (line.length() > 0)
                    this.executeSetupCommand(line);
            } catch (RestaurantException | RuntimeException e) {
                Out.println(e.getMessage());
            }
        }

            bufferedReader.close();

        return this.getRestaurant();
    }
}