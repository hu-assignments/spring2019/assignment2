package exceptions;

/**
 * Thrown to indicate that maximum number of {@code Order}s a {@code Table} can have
 * has been reached and there can be no further {@code Order}s to this {@code Table}
 * before a <em>checkout</em> is requested.
 */
public class TooManyOrdersException extends RestaurantException {

    /**
     * Constructs a {@code TooManyOrdersException} with a default detail message.
     */
    public TooManyOrdersException() {
        super("Not allowed to exceed max number of orders!");
    }
}
