package exceptions;

/**
 * Thrown to indicate that base command is not recognized.
 */
public class InvalidCommandException extends IllegalArgumentException {

    /**
     * Constructs {@code InvalidCommandException} with a default message.
     *
     * @param command Full command that is not recognized.
     */
    public InvalidCommandException(String command) {
        super(String.format("The command \"%s\" is invalid.", command));
    }
}
