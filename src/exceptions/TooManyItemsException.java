package exceptions;

import static util.Constants.MAX_ITEM_PER_ORDER;

/**
 * Thrown to indicate that maximum number of {@code Item}s in a single {@code Order} has been reached,
 * and there can be no further additions to this {@code Order}.
 */
public class TooManyItemsException extends RestaurantException {


    /**
     * Constructs a {@code TooManyItemsException} with a default detail message.
     */
    public TooManyItemsException() {
        super(String.format("Not allowed to exceed %d items in a single order!", MAX_ITEM_PER_ORDER));
    }
}
