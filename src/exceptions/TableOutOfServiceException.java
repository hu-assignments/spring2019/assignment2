package exceptions;


/**
 * Thrown to indicate that a <em>checkout</em> or <em>add order</em> operation
 * is about to undergo on a table that is out of service.
 */
public class TableOutOfServiceException extends RestaurantException {

    /**
     * Constructs a {@code TableOutOfServiceException} with a default detail message.
     *
     * @param id ID of the {@code Table} that is out of service.
     */
    public TableOutOfServiceException(int id) {
        super(String.format("Table (= ID %d) is not in service at the moment.", id));
    }

    /**
     * Constructs a {@code TableOutOfServiceException} with a default detail message.
     *
     * @param name Name of the {@code Waiter}.
     */
    public TableOutOfServiceException(String name) {
        super(String.format("This table is either not in service now or %s cannot be assigned this table!", name));
    }
}
