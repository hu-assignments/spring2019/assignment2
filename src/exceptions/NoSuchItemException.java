package exceptions;

/**
 * Thrown to indicate that no {@code Item} with specified name
 * can be found in the {@code Restaurant}'s <b>initial</b> stock.
 */
public class NoSuchItemException extends RestaurantException {

    /**
     * Constructs a {@code NoSuchItemException} with a default detail message.
     *
     * @param itemName Name of the item that is unknown.
     */
    public NoSuchItemException(String itemName) {
        super(String.format("Unknown item %s", itemName));
    }
}
