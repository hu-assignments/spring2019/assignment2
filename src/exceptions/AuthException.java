package exceptions;

/**
 * Thrown to indicate that the specified waiter does not have permission to operate on the table.
 *
 * <p>In order for waiters to have permission, they should be assigned to the same table
 * previously, when taking a new order.
 */
public class AuthException extends RestaurantException {

    /**
     * Constructs a {@code AuthException} with generalized
     * message that contains {@code Waiter}'s name.
     *
     * @param name Name of the {@code Waiter}.
     */
    public AuthException(String name) {
        super(String.format("This table is either not in service now or %s cannot be assigned this table!", name));
    }
}
