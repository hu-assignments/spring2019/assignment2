package exceptions;

/**
 * Thrown to indicate that the {@code Worker} with the specified name
 * already exists in the {@code Restaurant}.
 */
public class WorkerAlreadyExistsException extends RestaurantException {

    /**
     * Constructs a {@code WorkerAlreadyExistsException} with default detail message.
     *
     * @param title Title of the {@code Worker}.
     * @param name  Name of the {@code Worker}.
     */
    public WorkerAlreadyExistsException(String title, String name) {
        super(String.format("The %s with the name \"%s\" already exists.", title, name));
    }
}