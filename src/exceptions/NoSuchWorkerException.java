package exceptions;

/**
 * Thrown to indicate that no {@code Worker} with specified name
 * can be found among {@code Restaurant}'s staff.
 */
public class NoSuchWorkerException extends RestaurantException {

    /**
     * Constructs a {@code NoSuchWorkerException} with specified detail message.
     *
     * @param title      Title of the {@code Worker}.
     * @param workerName Name of the {@code Worker} that does not exist.
     */
    public NoSuchWorkerException(String title, String workerName) {
        super(String.format("There is no %s named %s", title, workerName));
    }
}
