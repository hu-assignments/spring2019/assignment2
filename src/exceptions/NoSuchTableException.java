package exceptions;

/**
 * Thrown to indicate that a {@code Table} with given ID cannot be found in
 * {@code Restaurant}'s {@code Table}'s.
 */
public class NoSuchTableException extends RestaurantException {

    /**
     * Constructs a {@code NoSuchTableException} with default detail message.
     *
     * @param id ID of the table that is not present among {@code Restaurant}'s tables.
     */
    public NoSuchTableException(int id) {
        super(String.format("There's no table with ID %d.", id));
    }
}
