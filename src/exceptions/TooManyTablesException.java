package exceptions;

import restaurant.Employer;
import restaurant.Worker;

/**
 * Thrown to indicate that the maximum number of {@code Table}s a {@code Restaurant}
 * can have, or an {@code Employer} can create, has been reached.
 */
public class TooManyTablesException extends RestaurantException {

    /**
     * Constructs a {@code TooManyTablesException} for a {@code Restaurant} that
     * has reached its maximum number of {@code Table}s.
     *
     * @param max Maximum number of {@code Table}s a {@code Restaurant} can have.
     */
    public TooManyTablesException(int max) {
        super(String.format("Not allowed to exceed max. number of tables, %d", max));
    }

    /**
     * Constructs a {@code TooManyTablesException} for a {@code Worker} that has reached
     * it's maximum number of {@code Table}s they can operate or create.
     *
     * @param title      Title of the {@code Worker}.
     * @param workerName Name of the {@code Worker}.
     * @param max        Maximum number of {@code Table}s they can operate or create.
     */
    public TooManyTablesException(String title, String workerName, String operation, int max) {
        super(String.format("The %s %s %s %d tables!", title, workerName, operation, max));
    }

    /**
     * Constructs a {@code TooManyTablesException} for a {@code Worker} that has reached
     * it's maximum number of {@code Table}s they can operate or create.
     *
     * @param worker Name of the {@code Worker}.
     * @param max    Maximum number of {@code Table}s they can operate or create.
     */
    public TooManyTablesException(Worker worker, int max) {
        super(
                (worker instanceof Employer) ?
                        String.format("%s has already created %d tables!", worker.getName(), max) :
                        String.format("Not allowed to service max. number of tables, %d", max)
        );
    }
}
