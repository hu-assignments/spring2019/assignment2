package exceptions;

/**
 * Thrown to indicate that no {@code Item} with specified name
 * can be found in the {@code Restaurant}'s stock.
 */
public class NoItemInStockException extends RestaurantException {

    /**
     * Constructs a {@code NoItemInStockException} with a default detail message.
     *
     * @param itemName Name of the item that can't be found.
     */
    public NoItemInStockException(String itemName) {
        super(String.format("Sorry! No %s in the stock!", itemName));
    }
}
