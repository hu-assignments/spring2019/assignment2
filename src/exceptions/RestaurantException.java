package exceptions;

/**
 * Parent class of all exceptions related to {@code Restaurant} and it's operations.
 *
 * <p>Thrown to indicate that an action outside of the way
 * the {@code Restaurant} should operate has been requested.
 */
public class RestaurantException extends Exception {

    /**
     * Constructs a {@code RestaurantException} with specified message.
     *
     * @param message Detail message.
     */
    public RestaurantException(String message) {
        super(message);
    }
}
