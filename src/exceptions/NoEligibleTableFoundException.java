package exceptions;

/**
 * Thrown to indicate that no eligible {@code Table} found in {@code Restaurant} that
 * satisfies the number of customers.
 *
 * <p>In order for a {@code Table} to be eligible, it must not be in service,
 * and it should have a capacity that can hold the number of customers specified in the order.
 */
public class NoEligibleTableFoundException extends RestaurantException {

    /**
     * Constructs a {@code NoEligibleTableFound} with default message.
     */
    public NoEligibleTableFoundException() {
        super("There is no appropriate table for this order!");
    }
}
