package exceptions;

/**
 * Thrown to indicate that maximum number of {@code Worker}s that a
 * {@code Restaurant} can have has been reached, and no further additions can be done.
 */
public class TooManyWorkersException extends RestaurantException {

    /**
     * Constructs a {@code TooManyWorkersException} with default detail message.
     *
     * @param title The title of the {@code Worker}.
     * @param max   Maximum number of {@code Worker}s a {@code Restaurant} can have.
     */
    public TooManyWorkersException(String title, int max) {
        super(String.format("Maximum number of %s (%d) has been reached.", title, max));
    }
}